reaper = {
    'black1': "#2E303E",
    'black2': "#14141b",
    'black3': "#1b1b25", #lighter
    'black4': "#0b0b0f", #darker
    'red1': "#fc7171",
    'red2': "#ff3334",
    'green1': "#5af78e",
    'green2': "#2cd665",
    'yellow1': "#f3f99d",
    'yellow2': "#fcb03f",
    'blue1': "#a6b8cc",
    'blue2': "#7aa6da",
    'magenta1': "#FF85B8",
    'magenta2': "#b77ee0",
    'cyan1': "#d67253",
    'cyan2': "#54ced6",
    'white1': "#EEFFFF",
    'white2': "#bbbbbb",

    'background': "#14141b",
    'foreground': "#a6b8cc",
    'highlight': "#FF85B8",
    'active': "#54ced6",
    'inactive': "#a6b8cc"
}